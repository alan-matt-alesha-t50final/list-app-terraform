# List App with Terraform
List App for frontend and backend deployment on AWS using Terraform.

### Description
The focus of the final AWS project was deploy an AWS Terraform full stack application deployment. The team decided to deploy the list-app used during prior class exercises in order to prioritize Terraform instead of coding.

## Requirements
1. Full stack public facing browser UI persistence with a database.
2. Terraform configuration - little to no console management. The team should be able to launch or destroy the infrastructure with one command. Key pairs should be created in the management console.
3. Custom VPC - utilize public and private subnets, security groups, route tables and internet gateways to ensure as little public facing parts of the stack are exposed as possible. Instances in private subnets will need a way out to the Internet via NAT Gateway.

## Design

### Architecture Diagram - Plan
The original design included both a public subnet with an EC2 instance to host the front-end UI and a private subnet with another EC2 instance that communicates with the DynamoDB to serve up the database contents to the front-end.
![](ArchitectureImage.PNG)

### Architecture Diagram - Actual
The team discovered a proxy would be needed in order for the front-end UI in the public subnet to communicate with the database server in the private subnet. Due to time constraints, the design was adjusted to place the database server into the public subnet instead of creating a proxy to keep it private. Below is the updated architecture diagram as a result of this decision.

![](ArchitectureImage_New.PNG)

## Deployment
The team deployed an application to an EC2 instance and connected it to the list-app stack which consists of two EC2 instances and a DynamoDB table:
   - list-app-react - the frontend ui
   - list-app-express-dynamodb - the backend api
   - the DynamoDB table: ama-list-app

The team then used user data scripts to launch the list-app stack.
We allowed our backend instance to connect to DynamoDB.

## Issues
While developing this project the team ran into some issues.
1. Deploying the front end.
    - Big issue with deploying the front end was figuring out the script to write to get 
2. Communicating to the backend API to store data on DynamoDB.
    - We forgot to include the git checkout of the feat/connect-express-api branch after cloning in the user_data script. This branch contains the necessary logic for the backend connection.
3. Writing the scripts to deploy the application.
4. Getting the IAM roles attached and working appropriately.

## Prompts from each Contributor

### Alan Andersen
1. Original goals and how they changed (if at all).
    We originally talked about trying to put up our Recipe Wizard app, or create a new one but with the limited time we settled on just deploying the existing list app React front end and Express back end.
2. What I learned.
    I learned that going through the management console as a reference is a great help when trying to write out the terraform file. The terraform documenation is great as well, because it has reference to every resource or data field that you would want. A large hurdle was trying to figure out what resources was actually needed but we were able to learn through small failures.

3. What I'd do next if given more time.
    We didn't have to store anything in an S3 bucket for this projet, so that is something I'd like to try. That would require a new IAM role with a policy to allow our EC2 instance to talk to our S3 bucket.

### Matt:
  1. Goal: To deploy the list app as part of a full stack deployment using Terraform. The only thing that really changed from original plan was having a NAT Gateway. The plan was to have a NAT Gateway but because our private subnet but our front end did not have a proxy to talk to the front end, so we scrapped the Nat Gateway and just used the public subnet.
  2. Learned: I learned more about deploying using Terraform and the code needed to make that happen.
  3. If we had more time: If we had more time I would like to have tried using the NAT Gateway from the private subnet.

### Alesha Roy
1. Original goals and how they changed (if at all)
    - Original goal: Deploy the 'Recipe Wizard' React app
      - Update project to persist data in a backend DynamoDB table
      - Research using AWS Secrets to store API keys
      - Etc.
    - Adjustments: Deploy the existing list-app
      - No coding needed
      - Full focus on writing the Terraform to deploy
2. What I learned
    - Terraform is great, but it's difficult to implement from scratch when learning
    - Terraform reference material is fantastic, but it can be difficult to decide which resource to configure or understand what all is available or needed
3. What I'd do next if given more time
    - Build a proxy server to enable communication with the private subnet instead of just moving the EC2 instance into the public subnet
    - Experiment with one of my own projects to get more practice with the entire process