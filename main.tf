terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
}

resource "aws_vpc" "ListAppVPC" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "ama-vpc"
  }
}

resource "aws_subnet" "public-subnet-frontend" {
  vpc_id                  = aws_vpc.ListAppVPC.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = "ama-public-subnet-frontend"
  }
}

resource "aws_subnet" "private-subnet-backend" {
  vpc_id     = aws_vpc.ListAppVPC.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "ama-private-subnet-backend"
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.ListAppVPC.id

  tags = {
    Name = "ama-gateway"
  }
}

resource "aws_route_table" "public-table" {
  vpc_id = aws_vpc.ListAppVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway.id
  }

  tags = {
    Name = "ama-public-route-table"
  }
}

resource "aws_vpc_endpoint_route_table_association" "vpc-endpoint-rta" {
  route_table_id  = aws_route_table.public-table.id
  vpc_endpoint_id = aws_vpc_endpoint.ama-endpoint.id
}

resource "aws_route_table_association" "association0" {

  subnet_id      = aws_subnet.public-subnet-frontend.id
  route_table_id = aws_route_table.public-table.id

}

resource "aws_security_group" "ama-web-dmz" {
  name        = "ama-web-dmz"
  description = "security group for our web app"
  vpc_id      = aws_vpc.ListAppVPC.id

  ingress {
    description = "HTTP"
    protocol    = "tcp"
    to_port     = 80
    from_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    protocol    = "tcp"
    to_port     = 22
    from_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    protocol    = "tcp"
    to_port     = 5000
    from_port   = 5000
    cidr_blocks = ["10.0.2.0/24"]
  }


  egress {
    description = "all traffic out"
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ama-web-dmz"
  }

}

resource "aws_security_group" "ama-DBSG" {
  name        = "ama-DBSG"
  description = "security group for our db connection"
  vpc_id      = aws_vpc.ListAppVPC.id

  ingress {
    description = "HTTP"
    protocol    = "tcp"
    to_port     = 80
    from_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    protocol    = "tcp"
    to_port     = 5000
    from_port   = 5000
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    protocol    = "tcp"
    to_port     = 22
    from_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "All ICMP -IPv4"
    protocol    = "icmp"
    to_port     = -1
    from_port   = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "all traffic out"
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ama-DBSG"
  }

}

data "aws_ami" "amazon-2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "web" {
  ami                         = data.aws_ami.amazon-2.id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public-subnet-frontend.id
  vpc_security_group_ids      = [aws_security_group.ama-web-dmz.id]
  key_name                    = "ama_final"
  user_data                   = <<EOF
    #! /bin/bash
  sudo yum update -y
  su - ec2-user -c "curl https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash"
  su - ec2-user -c "nvm install 16.15.1"
  su - ec2-user -c "nvm use 16.15.1"
  . /home/ec2-user/.nvm/nvm.sh
  . /home/ec2-user/.bashrc
  sudo yum install git -y
  git clone https://github.com/gSchool/list-app-react.git && cd list-app-react && git checkout feat/connect-express-api && npm install
  echo REACT_APP_API_URL=http://${aws_instance.db.public_ip}:5000 > .env
  npm run build
  npm install -g serve
  sudo yum install libcap-devel -y
  sudo setcap cap_net_bind_service=+ep /home/ec2-user/.nvm/versions/node/v16.15.1/bin/node
  serve -s build -l 80
  EOF

  tags = {
    Name = "ama-webserver"
  }

  depends_on = [
    aws_instance.db
  ]
}

resource "aws_instance" "db" {
  ami           = data.aws_ami.amazon-2.id
  instance_type = "t2.micro"
  # subnet_id              = aws_subnet.private-subnet-backend.id
  subnet_id              = aws_subnet.public-subnet-frontend.id
  vpc_security_group_ids = [aws_security_group.ama-DBSG.id]
  key_name               = "ama_final"
  iam_instance_profile   = "${aws_iam_instance_profile.db-profile.name}"
  user_data              = <<EOF
  #! /bin/bash 
  sudo yum update -y
    su - ec2-user -c "curl https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash"
  su - ec2-user -c "nvm install 16.15.1"
  su - ec2-user -c "nvm use 16.15.1"
  . /home/ec2-user/.nvm/nvm.sh
  . /home/ec2-user/.bashrc
  sudo yum install git -y
  git clone https://gitlab.com/alan-matt-alesha-t50final/list-app-backend.git && cd list-app-backend && npm install
  echo -e 'PORT=5000\nTABLE_NAME=ama-list-app\nAPP_ID=0\nAWS_DEFAULT_REGION=us-east-2' > .env
  npm start
  EOF

  tags = {
    Name = "ama-DBserver"
  }
}

# Create dynamoDB
resource "aws_dynamodb_table" "ama-dynamodb-table" {
  name           = "ama-list-app"
  billing_mode   = "PROVISIONED"
  write_capacity = 5
  read_capacity  = 5
  hash_key       = "id"

  attribute {
    name = "id"
    type = "N"
  }

  tags = {
    Name        = "ama-list-app"
    Environment = "dev"
  }
}

# connect VPC endpoint

resource "aws_vpc_endpoint" "ama-endpoint" {
  vpc_id          = aws_vpc.ListAppVPC.id
  service_name    = "com.amazonaws.us-east-2.dynamodb"
  route_table_ids = [aws_route_table.public-table.id]

  tags = {
    Name = "ama-endpoint"
  }
}

#create iam role policy

data "aws_iam_policy_document" "policy" {
  statement {
    actions = [
          "dynamodb:List*",
          "dynamodb:DescribeReservedCapacity*",
          "dynamodb:DescribeLimits",
          "dynamodb:DescribeTimeToLive"
        ]
    resources = ["*"]
    effect = "Allow"
  }

  statement {
    effect = "Allow"
    actions = [
          "dynamodb:BatchGet*",
          "dynamodb:DescribeStream",
          "dynamodb:DescribeTable",
          "dynamodb:Get*",
          "dynamodb:Query",
          "dynamodb:Scan",
          "dynamodb:BatchWrite*",
          "dynamodb:CreateTable",
          "dynamodb:Delete*",
          "dynamodb:Update*",
          "dynamodb:PutItem"
        ]
    resources = [aws_dynamodb_table.ama-dynamodb-table.arn]
  }
  
}

resource "aws_iam_role_policy" "db-role" {
  role = aws_iam_role.db-role.id
  policy = data.aws_iam_policy_document.policy.json
}

#create IAM role 
resource "aws_iam_role" "db-role" {
  name = "ama-db-access"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  depends_on = [
    aws_dynamodb_table.ama-dynamodb-table
  ]

}


#create instance profile

resource "aws_iam_instance_profile" "db-profile" {
  name = "db-profile"
  role = aws_iam_role.db-role.name

}



output "web-ip" {
  value = aws_instance.web.public_ip
}

output "front-end-echo" {
  value = aws_instance.db.public_ip
}

# Below code is for when a proxy solution is created. Private subnet would be recreated and 
# backend EC2 instance would be launched there. Private route table and association to be 
# created.

# resource "aws_route_table" "private-table" {
#   vpc_id = aws_vpc.ListAppVPC.id
#   route {
#     cidr_block     = "0.0.0.0/0"
#     nat_gateway_id = aws_nat_gateway.private-NAT.id
#   }

#   tags = {
#     Name = "ama-private-route-table"
#   }
# }

# resource "aws_route_table_association" "private-rta" {
#   subnet_id      = aws_subnet.private-subnet-backend.id
#   route_table_id = aws_route_table.private-table.id
# }

# resource "aws_eip" "lb" {
#   vpc = true

#   depends_on = [aws_internet_gateway.gateway]
# }

# # Create and connect NAT gateway
# resource "aws_nat_gateway" "private-NAT" {
#   subnet_id     = aws_subnet.public-subnet-frontend.id
#   allocation_id = aws_eip.lb.id
# }